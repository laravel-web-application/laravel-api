<p align="center"><img src="https://res.cloudinary.com/dtfbvvkyp/image/upload/v1566331377/laravel-logolockup-cmyk-red.svg" width="400"></p>

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/d/total.svg" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/v/stable.svg" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/license.svg" alt="License"></a>
</p>

## About Laravel

Laravel is a web application framework with expressive, elegant syntax. We believe development must be an enjoyable and creative experience to be truly fulfilling. Laravel takes the pain out of development by easing common tasks used in many web projects, such as:

- [Simple, fast routing engine](https://laravel.com/docs/routing).
- [Powerful dependency injection container](https://laravel.com/docs/container).
- Multiple back-ends for [session](https://laravel.com/docs/session) and [cache](https://laravel.com/docs/cache) storage.
- Expressive, intuitive [database ORM](https://laravel.com/docs/eloquent).
- Database agnostic [schema migrations](https://laravel.com/docs/migrations).
- [Robust background job processing](https://laravel.com/docs/queues).
- [Real-time event broadcasting](https://laravel.com/docs/broadcasting).

Laravel is accessible, powerful, and provides tools required for large, robust applications.

## Laravel API with Resource
### Things to do
1. Clone this repository: `git clone https://gitlab.com/laravel-web-application/laravel-api.git`
2. Go inside the folder: `cd laravel-api`
4. Run `composer install`
5. Run `composer dump-autoload`
6. Run `cp .env.example .env`
7. Put your DB Name & DB Credentials in `.env` file
8. Run `php artisan key:generate`
9. Run `php artisan migrate:refresh --seed`
10. Run `php artisan serve`
11. Open your favorite REST Client App (I choose POSTMAN)
12. Import POSTMAN Collection file.

### Screen shot

Add New Article

![Add New Article](img/add.png "Add New Article")

List All Articles

![List All Articles](img/list.png "List All Articles")

Get Article By ID

![Get Article By ID](img/find.png "Get Article By ID")

Update Article By ID

![Update Article By ID](img/update.png "Update Article By ID")

Delete Article By ID

![Delete Article By ID](img/delete.png "Delete Article By ID")
 
